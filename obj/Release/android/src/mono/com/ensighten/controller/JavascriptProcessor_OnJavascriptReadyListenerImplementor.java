package mono.com.ensighten.controller;


public class JavascriptProcessor_OnJavascriptReadyListenerImplementor
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		com.ensighten.controller.JavascriptProcessor.OnJavascriptReadyListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onJavascriptReady:()V:GetOnJavascriptReadyHandler:Com.Ensighten.Controller.JavascriptProcessor/IOnJavascriptReadyListenerInvoker, EnsightenAndroid\n" +
			"";
		mono.android.Runtime.register ("Com.Ensighten.Controller.JavascriptProcessor+IOnJavascriptReadyListenerImplementor, EnsightenAndroid", JavascriptProcessor_OnJavascriptReadyListenerImplementor.class, __md_methods);
	}


	public JavascriptProcessor_OnJavascriptReadyListenerImplementor ()
	{
		super ();
		if (getClass () == JavascriptProcessor_OnJavascriptReadyListenerImplementor.class)
			mono.android.TypeManager.Activate ("Com.Ensighten.Controller.JavascriptProcessor+IOnJavascriptReadyListenerImplementor, EnsightenAndroid", "", this, new java.lang.Object[] {  });
	}


	public void onJavascriptReady ()
	{
		n_onJavascriptReady ();
	}

	private native void n_onJavascriptReady ();

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
